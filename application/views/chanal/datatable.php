<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Channels
			<small>list</small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
              <h3 class="box-title">Channel Mangement</h3>
							<a class="btn btn-app" id="addch" href="#animatedModal">
								<i class="fa fa-plus-square" aria-hidden="true"></i> Add
							</a>
          </div>
					<div class="box-body">
						<table id="ch_table" class="table table-bordered table-striped">
							<thead>
							<tr>
                <th>
                  Id
                </th>
								<th>Name </th>
								<th class="no-sort">Description</th>
								<th>Created_at</th>
                <th class="no-sort">API_KEY</th>
								<th class="no-sort"></th>
							</tr>
							</thead>
							<tbody>
								<?php foreach ($ch as $key => $value): ?>
									<tr>
                    <td>
                        <?php echo $value->id ?>
                    </td>
										<td>
											<?php echo $value->name ?>
										</td>
										<td>
											<?php echo $value->description ?>
										</td>
										<td>
											<?php echo $value->created_at ?>
										</td>
                    <td>
                      <?php foreach ($value->api_keys as $k => $v): ?>
                        <ul>
                          <li class="list-group-item">
                            <?php echo $v->write_flag ? '<span class="label label-info">Write</span>' : '<span class="label label-info">Read</span>'  ?>
                             <?php echo $v->api_key; ?>
                           </li>
                        </ul>

                      <?php endforeach; ?>
                    </td>
										<td align="center">
											<div style="padding-top:7%">
												<a class="btn btn-app" href="<?php echo base_url() ?>channel/view?ch_id=<?php echo x_encode($value->id) ?>">
					                <i class="fa fa-external-link" aria-hidden="true"></i> View
					              </a>
												<a class="btn btn-app" href="<?php echo base_url() ?>project/view/">
					                <i class="fa fa-edit" aria-hidden="true"></i> Edit
					              </a>
												<a deletebtn class="btn btn-app" href="<?php echo base_url() ?>channel/delete?ch_id=<?php echo x_encode($value->id) ?>">
					                <i class="fa fa-trash-o" aria-hidden="true"></i>Delete
					              </a>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>

	</section>
	<!-- /.content -->
</div>


<div id="animatedModal">
			 <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->

			 <div class="modal-content" >
					 <div class="col-md-6 col-md-offset-3" style="padding-top:10%">
						 <div class="box box-primary">
							 <div class="box-header with-border">
								 <h1 class="box-title">Create Channel</h1>
							 </div>
							 <form action="<?php echo base_url()?>/channel/create" method="post">
								 <div class="box-body">

									 <div class="form-group">
										 <div class="input-group">
											 <span class="input-group-addon"><i class="glyphicon glyphicon-leaf"></i></span>
											 <input type="text" class="form-control" name="chname" placeholder="Channel Name">
										 </div>
									 </div>
									 <div class="form-group">
	                   <label>Description</label>
	                   <textarea class="form-control" rows="3" name="chdes" placeholder="Description"></textarea>
	                 </div>

								 </div>
								 <div class="box-footer">
									 <button type="submit" class="btn btn-primary">Submit</button>
									 <button type="button" class="btn btn-link close-animatedModal">Cancel</button>
								 </div>
							 </form>
						 </div>
					 </div>
			 </div>
	 </div>

<script type="text/javascript">
	$.fn.datepicker.defaults.format = "dd/mm/yyyy";
	$('#startdate,#endate').datepicker({
		autoclose: true
	});

	$('#ch_table').DataTable({
		"columnDefs": [ {
      "targets"  : 'no-sort',
      "orderable": false,
      "order": []
    }]
	});

	$("#addch").animatedModal({
		'animatedIn':'lightSpeedIn',
		'color': '#d2d6de'
	});


	$(document).on('click','a[deletebtn]', function(event) {
		event.preventDefault();
		var link = $(this);
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this channel",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel !",
			closeOnConfirm: false,
			closeOnCancel: false },
			function(isConfirm){
				if (isConfirm) {
					swal("Deleted!", "Your Channel  has been deleted.", "success");
					 window.location = link.attr('href');
				} else {
					swal("Cancelled", "Your Channel  is safe :)", "error");

				} });


	});



</script>
