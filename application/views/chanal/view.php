<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
			Channel View
			<small><?php echo $channal_info->id ?></small>
		</h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-3">
        <div class="box">
          <div class="box-body">
            <h3 class="profile-username text-center">Channal_ID <a href="https://thingspeak.com/channels/<?php echo $channal_info->id ?>"><?php echo $channal_info->id ?></h3>
            <p class="text-muted text-center">Name
              <?php echo $channal_info->name ?>
            </p>
            <ul class="list-group list-group-unbordered">
              <?php foreach ($channal_info as $key => $value): ?>
                <?php if($key == 'tags' or $key == 'name' or $key == 'id'){continue;} ?>
                  <li class="list-group-item">
                    <b><?php echo $key ?></b>
                    <a class="pull-right">
                      <?php echo $value ?>
                    </a>
                  </li>
                  <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-9">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="false">Channal Infomation</a></li>
            <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="true">Timeline</a></li>
            <li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">ThingSpeak(Humidity,Temperature)</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="activity">
              <!-- Post -->
              <div class="row">
                <div class="col-md-12">
                  <div class="box box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Humudity <span id="hu_t"></span></h3>
                    </div>
                    <div class="box-body ">
                      <div class="progress active">
                        <div id="hu_p" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                          <span class="sr-only">40% Complete (success)</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="box box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Temperature <span id="temp_t"></span></h3>
                    </div>
                    <div class="box-body ">
                      <div class="progress active">
                        <div id="temp_p" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                          <span class="sr-only">40% Complete (success)</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="box box-solid">
                    <div class="box-header with-border">
                      <h2>Statistics Infomation</h2>
                    </div>
                    <div class="box-body">
                      <table class="table">
                        <h2>Usage</h2>
                        <tr>
                          <td>
                            water ( 3 lite per hr.)
                          </td>
                          <td>
                            <?php echo $waterpump ?> ml.
                          </td>
                        </tr>
                      </table>
                      <?php foreach ($stat as $key => $v): ?>
                        <h2><?php echo $key ?></h2>
                        <table class="table">
                          <?php foreach ($v as $key => $k): ?>
                            <tr>
                              <td>
                                <b><?php echo $key ?></b>
                              </td>
                              <td>
                                <span><?php echo $k ?></span>
                              </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>

                        <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Post -->
              <div class="post clearfix">


              </div>
              <!-- /.post -->

              <!-- Post -->

            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="timeline">
              <!-- The timeline -->
              <ul class="timeline timeline-inverse">
                <!-- timeline time label -->
                <?php $isFirst=true; ?>
                  <?php foreach ($feeds as $key => $value): ?>
                    <?php if ($isFirst): ?>
                      <li class="time-label">
                        <span class="bg-red">
		                          <?php echo $value->created_at ?>
		                        </span>
                      </li>
                      <?php $isFirst=false; ?>
                        <?php endif; ?>
                          <li>
                            <i class="fa fa-comments bg-yellow"></i>

                            <div class="timeline-item">
                              <span class="time"><i class="fa fa-clock-o"></i><span class="timex"><?php echo $value->created_at; ?></span></span>

                              <h3 class="timeline-header">Device push data to thingspeak #<?php echo $value->entry_id ?></h3>

                              <div class="timeline-body">
                                <table class="table table-condensed">
                                  <tr>
                                    <td>
                                      field : 1
                                    </td>
                                    <td>
                                      <?php echo $value->field1 ?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      field : 2
                                    </td>
                                    <td>
                                      <?php echo $value->field2 ?>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                          </li>
                          <?php endforeach; ?>

              </ul>
            </div>
            <!-- /.tab-pane -->

            <div class="tab-pane" id="settings">
              <div class="post">
                <p class="lead">Field 1:</p>
                <div class="box-body" align="center">
                  <iframe width="500" height="250" style="border: 1px solid #cccccc;" src="http://thingspeak.com/channels/<?php echo $channal_info->id ?>/charts/1?dynamic=true"></iframe>
                </div>
              </div>
              <div class="post">
                <p class="lead">Field 2:</p>
                <div class="box-body" align="center">
                  <iframe width="500" height="250" style="border: 1px solid #cccccc;" src="http://thingspeak.com/channels/<?php echo $channal_info->id ?>/charts/2?dynamic=true"></iframe>
                </div>
              </div>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>




      </div>
    </div>

  </section>
  <!-- /.content -->
</div>

<script type="text/javascript">
  function getLocalDate(php_date) {
    var dt = new Date(php_date);
    var minutes = dt.getTimezoneOffset();
    dt = new Date(dt.getTime() + minutes * 60000);
    return (parseInt(dt.getHours()) + 7) + ":" + dt.getMinutes()
  }







  setInterval(function() {
    var chanal_id = '<?php echo $channal_info->id ?>';
    var url = 'https://api.thingspeak.com/channels/' + chanal_id + '/feeds/last.json';
    $.get(url, function(data) {
      $('#hu_t').text('( ' + data.field1 + " %" + ' )');
      $('#hu_p').attr('aria-valuenow', data.field1);
      $('#hu_p').width(data.field1 + '%');

      $('#temp_t').text('( ' + data.field2 + " c" + ' )');
      $('#temp_p').attr('aria-valuenow', data.field2);
      $('#temp_p').width(data.field2 + '%');
    });
  }, 2000);
</script>
