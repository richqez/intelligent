<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Project
			<small>Control panel</small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-header with-border">
              <h3 class="box-title"><small>CreateProjectForm</small></h3>
          </div>
					<form action="<?php echo base_url()?>/project/create" method="post">
						<div class="box-body">

							<div class="form-group">	

					<div class="input-group">
					 <span class="input-group-addon"><i class="glyphicon glyphicon-leaf"></i></span>
	                 <input type="text" class="form-control" id="name" name="name" placeholder="Project Name">
					</div>
              </div>
							<div class="form-group">
                 
                 
                 <div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-modal-window"></i></span>
	                <input type="text" class="form-control" id="chanal_id" name="chanal_id" placeholder="Chanal_id">
				 </div>
               
              </div>
							<div class="form-group">
                

                <div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-cloud"></i></span>
	                    <input type="text" class="form-control" id="read_api" name="read_api" placeholder="ThingSpeak Read API">
				</div>
           
              </div>
				<div class="form-group">
				 	<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
	                     <input type="text" class="form-control" id="startdate" name="startdate" placeholder="Start Date">
              		</div>
				</div>
                
				<div class="form-group">
               		<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
	                   	<input type="text" class="form-control" id="endate" name="enddate" placeholder="End Date">
              		</div>
               	</div>
						</div>
						<div class="box-footer">
                			<button type="submit" class="btn btn-primary">Submit</button>
                			<button type="button" class="btn btn-link">Cancel</button>
            			</div>
					</form>
				</div>
			</div>
		</div>

	</section>
	<!-- /.content -->
</div>

<script type="text/javascript">
$.fn.datepicker.defaults.format = "dd/mm/yyyy";
	$('#startdate,#endate').datepicker({
		autoclose: true
	});
</script>
