<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
			Project
			<small>Control panel</small>
		</h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Project Mangement</h3>
            <a class="btn btn-app" href="<?php echo base_url() ?>/project/add_page">
              <i class="fa fa-plus-square" aria-hidden="true"></i> Add
            </a>
          </div>
          <div class="box-body">
            <table id="project_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Project Name</th>
                  <th>Channel_id</th>
                  <th>thingspeak_api</th>
                  <th>startdate</th>
                  <th>enddate</th>
                  <th>action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($projects as $key => $value): ?>
                  <tr>
                    <td>
                      <?php echo $value->name ?>
                    </td>
                    <td>
                      <?php echo $value->chanal_id ?>
                    </td>
                    <td>
                      <?php echo $value->read_api ?>
                    </td>
                    <td>
                      <?php echo   dttoshow($value->startdate)?>
                    </td>
                    <td>
                      <?php echo   dttoshow($value->enddate) ?>
                    </td>
                    <td align="center">
                      <a valid-apikey class="btn btn-app" href="<?php echo base_url() ?>Chanal/index?projectkey=<?php echo x_encode($value->id) ?>">
                        <i class="fa fa-external-link" aria-hidden="true"></i> View
                      </a>
                      <a valid-apikey class="btn btn-app" href="<?php echo base_url() ?>project/view/<?php echo x_encode($value->id) ?>">
                        <i class="fa fa-edit" aria-hidden="true"></i> Edit
                      </a>
                      <a valid-apikey class="btn btn-app" href="<?php echo base_url() ?>project/view/<?php echo x_encode($value->id) ?>">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>Delete
                      </a>
                    </td>
                  </tr>
                  <?php endforeach; ?>
              </tbody>
            </table>
          </div>


        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>

<script type="text/javascript">
  $.fn.datepicker.defaults.format = "dd/mm/yyyy";
  $('#startdate,#endate').datepicker({
    autoclose: true
  });

  $('#project_table').DataTable({

  });


  $(document).on('click', 'a[valid-apikey]', function(event) {
    event.preventDefault();
    alert();
  });
</script>
