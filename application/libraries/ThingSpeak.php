<?php

class ThingSpeak
{

  private $api_uri = "https://api.thingspeak.com/channels/CHANNEL_ID/fields/FIELD_ID.json";
  private $api_key = "";
  private $channal_id = "";


  function __construct()
  {

  }

  public function valid_apikey($key){
    $url = 'https://api.thingspeak.com/channels.json?api_key=' . $key ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$url);
    $content = curl_exec($ch);
    return $content;
  }

  public function createChnannel($apikey,$chname,$chdes){

    $url = "https://api.thingspeak.com/channels";

    $frmData = [
      'api_key'     => urlencode($apikey),
      'description' => urlencode($chdes),
      'name'        => urlencode($chname),
      'field1'      => urlencode('Humidity'),
      'field2'      => urlencode('Temperature'),
      'field3'      => urlencode('WaterStatus'),
      'field4'      => urlencode('FanStatus'),
      'public_flag' => urlencode('true')
    ];

    $fields_string = '';

    foreach ($frmData as $key => $value) {
       $fields_string .= $key.'='.$value.'&';
    }
    rtrim($fields_string, '&');

    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($frmData));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
  }


  public function deleteChannel($apikey,$chid){
    $url = 'https://api.thingspeak.com/channels/' . $chid .'.json?api_key=' . $apikey;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $result = curl_exec($ch);
    curl_close($ch);
    return $result ;
  }


  public function getMyChannalList($apikey){
    $url = "https://api.thingspeak.com/channels.json?api_key=".$apikey;
    //return $this->fetch("https://api.thingspeak.com/channels.json?api_key=2RYPDP2SU7DEL17G");
    return json_decode(file_get_contents('https://api.thingspeak.com/channels.json?api_key=' . $apikey));
  }


  public function setApi($api_key){
    $this->api_key = $api_key;
    return $this ;
  }
  public function setChannal($channal_id){
    $this->channal_id = $channal_id;
    return $this ;
  }

  private function fetch($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$url);
    $content = curl_exec($ch);
    return json_decode($content);
  }


  public function get_channal_info(){
    $url = 'http://api.thingspeak.com/channels/' . $this->channal_id  . '.json';
    return $this->fetch($url);
  }

  public function get_all_feeds($resultSize){
    $url = 'http://api.thingspeak.com/channels/' . $this->channal_id . '/feeds.json?results='. $resultSize.
    "&timezone=Asia/Bangkok";
    return $this->fetch($url);
  }





}















 ?>
