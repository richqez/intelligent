<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends SheepCode_Controller{


  private $path  = [
       "datatable" => "project/datatable",
       "addpage"=> "project/add",
       "view" => "project/view"
     ];

  function __construct()
  {
   	parent::__construct();
    set_secure_zone();
    $this->load->model('Project_model');
  }



	public function index()
	{

    $projects = $this->Project_model->findAll();
		$this->render($this->path['datatable'],["projects" => $projects]);

	}

  public function add_page(){
    $this->render($this->path['addpage']);
  }

  public function view(){

    $project = $this->Project_model->findById();

    $feeds =$this->thingspeak
                  ->setApi($project->read_api)
                  ->setChannal($project->chanal_id)
                  ->get_all_feeds(200);

    $chanal_info =$this->thingspeak
                  ->setApi($project->read_api)
                  ->setChannal($project->chanal_id)
                  ->get_channal_info();

    // prepare ds

    $ds_humudity = [];
    $ds_temperature = [];
    $cout_waterpump = 0;


    foreach ($feeds->feeds as $key => $value) {
        array_push($ds_humudity,$value->field1);
        array_push($ds_temperature,$value->field2);
        if ($value->field1 > 60) {
          $cout_waterpump++;
        }
    }



    $wateruse = ((($cout_waterpump  * 10 ) / 60) /60);

    //echo $wateruse;

    //
    $this->statistics->addSet($ds_humudity);
    $mean_humudity = $this->statistics->mean;
    $std_humudity = $this->statistics->getStdDeviation();
    $median_humudity = $this->statistics->median;

    //
    $this->statistics->addSet($ds_temperature);
    $mean_temperature = $this->statistics->mean;
    $std_temperature  = $this->statistics->getStdDeviation();
    $median_temperature = $this->statistics->median;



    $statistics_info = [
      'Temperature'  =>[
        'mean'       => $mean_humudity,
        'std'        => $std_humudity,
        'median'     => $median_humudity
      ],
      'Humudity'     => [
        'mean'       => $mean_temperature,
        'std'        => $std_temperature,
        'median'     => $median_temperature

      ],
    ];






  $this->render($this->path['view'],[
      "channal_info" => $chanal_info,
      "project"      => $project,
      "feeds"        => $feeds->feeds,
      "stat"         => $statistics_info,
      "waterpump"    => $cout_waterpump

    ]);

  }

  public function create(){
    $this->Project_model->create();
  }



  public function valid_apikey(){

    $project = $this->Project_model->findById();



  }


}
