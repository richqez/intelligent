<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chanal extends SheepCode_Controller{



  private $path  = [
       "datatable" => "chanal/datatable",
       "addpage"   => "Chanal/add",
       "view"      => "chanal/view"
     ];

  function __construct()
  {
   	parent::__construct();
    set_secure_zone();
    $this->load->model('Project_model');
   }

  public function index() {
    $this->render($this->path['datatable'],[ 'ch' =>
    $this->thingspeak->getMyChannalList($this->session->userdata('account_apikey')) ]);

  }


  public function create(){

    $chname = $this->input->post('chname');
    $chdes  = $this->input->post('chdes');
    $r      = $this->thingspeak->createChnannel(
      $this->session->userdata('account_apikey'),
      $chname,
      $chdes
    );

    redirect('channels','refresh');

  }

  public function delete(){
    $chid = x_decode($this->input->get('ch_id'));
    $r    = $this->thingspeak->deleteChannel($this->session->userdata('account_apikey'),$chid);
    redirect('channels',true);
  }

  public function view(){

    $channel_id = x_decode($this->input->get('ch_id'));


    $feeds = $this->thingspeak
                  ->setChannal($channel_id)
                  ->get_all_feeds(200);

    $chanal_info = $this->thingspeak
                       ->setChannal($channel_id)
                       ->get_channal_info();


    $ds_humudity    = [];
    $ds_temperature = [];
    $cout_waterpump = 0;

    if (sizeof($feeds) > 0) {
      foreach ($feeds->feeds as $key => $value) {
          array_push($ds_humudity,$value->field1);
          array_push($ds_temperature,$value->field2);
          if ($value->field1 > 60) {
            $cout_waterpump++;
          }
      }
    }

    $wateruse = ((($cout_waterpump  * 10 ) / 60) /60);
    //echo $wateruse;

    //
    if (sizeof($ds_humudity) > 0 and sizeof($ds_temperature) > 0) {

        $this->statistics->addSet($ds_humudity);
        $mean_humudity   = $this->statistics->mean;
        $std_humudity    = $this->statistics->getStdDeviation();
        $median_humudity = $this->statistics->median;

        //
        $this->statistics->addSet($ds_temperature);
        $mean_temperature   = $this->statistics->mean;
        $std_temperature    = $this->statistics->getStdDeviation();
        $median_temperature = $this->statistics->median;
    }

    $statistics_info = [
      'Temperature' =>[
        'mean'   => isset($mean_humudity) ? $mean_humudity     : 0 ,
        'std'    => isset($std_humudity) ? $std_humudity       : 0,
        'median' => isset($median_humudity) ? $median_humudity : 0
      ],
      'Humudity'    => [
        'mean'   => isset($mean_temperature) ? $mean_temperature     : 0 ,
        'std'    => isset($std_temperature) ? $std_temperature       : 0 ,
        'median' => isset($median_temperature) ? $median_temperature : 0

      ],
    ];

    $this->render($this->path['view'],[
        "channal_info" => $chanal_info,
        "feeds"        => isset($feeds->feeds) ? $feeds->feeds : [],
        "stat"         => $statistics_info,
        "waterpump"    => $cout_waterpump

      ]);


  }














}








 ?>
