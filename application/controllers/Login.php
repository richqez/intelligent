<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends SheepCode_Controller{



  private $path  = [
       "login_page" => "login/login",
       "addpage"    => "Chanal/add",
       "view"       => "Chanal/view"
     ];

  function __construct()
  {
   	parent::__construct();
    
  }



  public function index()
  {
    $this->render_one($this->path['login_page']);
  }

  public function login(){

    $account_apikey = $this->input->post('apikey');

    $channls = json_decode(file_get_contents('https://api.thingspeak.com/channels.json?api_key=' . $account_apikey));

    $sessionData = [

      'isLogged'       => true ,
      'account_apikey' => $account_apikey,
      'channels'       => $channls,

    ];

    $this->session->set_userdata($sessionData);

    redirect('channels','refresh');


  }

  public function logout(){
    $this->session->sess_destroy();
    redirect('','refresh');
  }















}








 ?>
