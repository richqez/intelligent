<?php


//06/11/2016

function dttoshow($source){

	$splitSource = explode("-", $source);
  $dd = $splitSource[2];
  $mm = $splitSource[1];
  $yyyy = $splitSource[0];
  return  $dd. '-' . $mm . '-' . $yyyy ;

}

function dttosave($source){

	$splitSource = explode("/", $source);
  $dd = $splitSource[0];
  $mm = $splitSource[1];
  $yyyy = $splitSource[2];
  return  $yyyy. '-' . $mm . '-' . $dd ;

}


 function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

 function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
function x_encode($value){
			$CI =& get_instance();
			$key= $CI->config->item('encryption_key');
	    if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv);
        return trim(safe_b64encode($crypttext));
    }
function x_decode($value){
				$CI =& get_instance();
				$key= $CI->config->item('encryption_key');
        if(!$value){return false;}
        $crypttext = safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }


		function isLogin(){

		    $CI =& get_instance();

		    if ($CI->session->userdata('isLogged') != NULL and $CI->session->userdata('isLogged') ) {
		      return TRUE;
		    }
		    else{
		      return FALSE;
		    }


		  }

		  function set_secure_zone(){
		    //$CI =& get_instance();
		    if (!isLogin()) {
		      redirect('login','refresh');
		    }
		  }



 ?>
